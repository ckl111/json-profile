package com.example.fastjsontest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastjsonTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FastjsonTestApplication.class, args);
	}

}
