package com.example.fastjsontest.annotation;

/**
 * 需要排除当前该注解指定的field
 */


import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ActiveFastJsonProfileInController {
    /**
     * 活跃的profile
     * @return
     */
    String profile();

    Class<?> clazz();
}
