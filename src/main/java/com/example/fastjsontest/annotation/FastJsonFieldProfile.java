package com.example.fastjsontest.annotation;


import java.lang.annotation.*;

/**
 * 在field上使用，表示该字段在指定profile激活时，序列化或者不进行序列化
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FastJsonFieldProfile {
    /**
     * 序列化为json字符串时，
     * 如果profileType为 {@link FastJsonFieldProfileType#INCLUDE}时，则该字段将会被序列化；
     * 如果profileType为 {@link FastJsonFieldProfileType#EXCLUDE}时，则该字段将不会被序列化；
     * @return
     */
    String[] profiles() default {};

    /**
     * profile的类型，分为包含和排除
     *
     * @return
     */
    FastJsonFieldProfileType profileType();
}
