package com.example.fastjsontest.annotation;

/**
 * Created by Administrator on 2019/11/9.
 */
public enum FastJsonFieldProfileType {
    /**
     * 该类型的field将会被序列化
     */
    INCLUDE,
    /**
     * 该类型的field不会被序列化
     */
    EXCLUDE
}
