package com.example.fastjsontest.controller;

import com.example.fastjsontest.annotation.ActiveFastJsonProfileInController;
import com.example.fastjsontest.message.CommonMessage;
import com.example.fastjsontest.model.User;
import com.example.fastjsontest.model.UserForExcludeTest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/11/9.
 */
@RestController
@RequestMapping("/exclude")
public class TestExcludeController {

    /**
     * 测试exclude类型的profile，这里指定了：
     * 激活profile为 excludeProfile
     * UserForExcludeTest中，对应的field将不会被序列化
     */
    @GetMapping("/test.do")
    @ActiveFastJsonProfileInController(profile = "excludeProfile",clazz = UserForExcludeTest.class)
    public CommonMessage<UserForExcludeTest> test() {
        UserForExcludeTest user = new UserForExcludeTest();
        user.setId(111L);
        user.setUserName("kkk");
        user.setAge(3);
        user.setHeight(165);

        CommonMessage<UserForExcludeTest> message = new CommonMessage<>();
        message.setCode("0000");
        message.setDesc("成功");
        message.setData(user);

        return message;
    }
}
