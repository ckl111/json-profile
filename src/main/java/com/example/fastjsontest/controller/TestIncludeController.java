package com.example.fastjsontest.controller;

import com.example.fastjsontest.annotation.ActiveFastJsonProfileInController;
import com.example.fastjsontest.message.CommonMessage;
import com.example.fastjsontest.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/11/9.
 */
@RestController
@RequestMapping("/include")
public class TestIncludeController {

    /**
     * 测试include类型的profile，这里指定了：
     * 激活profile为 includeProfile
     * User中，对应的field将会被序列化，其他字段都不会被序列化
     */
    @GetMapping("/test.do")
    @ActiveFastJsonProfileInController(profile = "includeProfile",clazz = User.class)
    public CommonMessage<User> test() {
        User user = new User();
        user.setId(111L);
        user.setAge(8);
        user.setUserName("kkk");
        user.setHeight(165);

        CommonMessage<User> message = new CommonMessage<>();
        message.setCode("0000");
        message.setDesc("成功");
        message.setData(user);

        return message;
    }
}
