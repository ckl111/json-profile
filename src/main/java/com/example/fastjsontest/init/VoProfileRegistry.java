package com.example.fastjsontest.init;

import com.example.fastjsontest.annotation.ActiveFastJsonProfileInController;
import lombok.Data;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Administrator on 2019/11/9.
 */
@Data
public class VoProfileRegistry {
    private ConcurrentHashMap<String,ActiveFastJsonProfileInController> hashmap = new ConcurrentHashMap<String,ActiveFastJsonProfileInController>();

}
