package com.example.fastjsontest.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Administrator on 2019/11/9.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonMessage<T> {
    /**
     * 消息码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;

    /**
     * 数据
     */
    private T data;

}
