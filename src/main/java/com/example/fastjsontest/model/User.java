package com.example.fastjsontest.model;

import com.example.fastjsontest.annotation.FastJsonFieldProfile;
import com.example.fastjsontest.annotation.FastJsonFieldProfileType;
import lombok.Data;

/**
 * Created by Administrator on 2019/11/9.
 */
@Data
public class User {
    @FastJsonFieldProfile(profiles = {"includeProfile"},profileType = FastJsonFieldProfileType.INCLUDE)
    private Long id;

    private String userName;

    private Integer age;

    @FastJsonFieldProfile(profiles = {"includeProfile"},profileType = FastJsonFieldProfileType.INCLUDE)
    private Integer height;
}
