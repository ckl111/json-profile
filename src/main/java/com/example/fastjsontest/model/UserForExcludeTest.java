package com.example.fastjsontest.model;

import com.example.fastjsontest.annotation.FastJsonFieldProfile;
import com.example.fastjsontest.annotation.FastJsonFieldProfileType;
import lombok.Data;

/**
 * Created by Administrator on 2019/11/9.
 */
@Data
public class UserForExcludeTest {
    private Long id;

//    @FastJsonFieldProfile(profiles = {"excludeProfile"},profileType = FastJsonFieldProfileType.EXCLUDE)
    private String userName;

//    @FastJsonFieldProfile(profiles = {"excludeProfile"},profileType = FastJsonFieldProfileType.EXCLUDE)
    private Integer age;

    private Integer height;
}
